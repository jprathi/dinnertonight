This is a Spring Boot Application.

Maven needs to be setup as a pre-requisite.

To run the application, follow the below steps

1. Run -> cmd
2. cd into dinnertonight folder
3. build the application using command "mvn package"
3. Run the application with the command "java -jar target\dinner-tonight-0.0.1-SNAPSHOT.jar fridge.csv recipes.json"
4. Open web browser and go to http://localhost:8080/tonis-kitchen/suggestion?date=24/06/2015

NOTE: the date quert parameter in is optional. Current date is used by default.