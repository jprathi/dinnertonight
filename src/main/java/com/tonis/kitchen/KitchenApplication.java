package com.tonis.kitchen;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.tonis.kitchen.domain.Recipe;
import com.tonis.kitchen.domain.Stock;
import com.tonis.kitchen.repository.RecipeRepository;
import com.tonis.kitchen.repository.StockRepository;
import com.tonis.kitchen.utils.RecipeJSONReader;
import com.tonis.kitchen.utils.StocksCSVReader;

@SpringBootApplication
public class KitchenApplication {
	private static final Logger LOG = LoggerFactory.getLogger(KitchenApplication.class);
	private static List<Stock> stocks;
	private static List<Recipe> recipes;

	public static void main(final String[] args) {
		if (args.length == 2) {
			try {
				loadStocks(args[0]);
				loadRecipes(args[1]);
				SpringApplication.run(KitchenApplication.class, args);
			} catch (final Exception e) {
				LOG.error("Error loading receipes/stocks. Please restart the application " + "with valid Fridge CSV and Recipes JSON files");
			}
		} else {
			LOG.info("Please provide 1-> fidge CSV and 2-> recipes JSON files as arguments");
		}

	}

	private static void loadRecipes(final String fileLocation) throws Exception {
		LOG.info("Recipes with json: {}", fileLocation);
		recipes = new RecipeJSONReader(fileLocation).read();
	}

	private static void loadStocks(final String fileLocation) throws Exception {
		LOG.info("Stocks with csv: {}", fileLocation);
		stocks = new StocksCSVReader(fileLocation).read();
	}

	@Bean
	public StockRepository stockRepository() {
		final StockRepository repository = new StockRepository();
		repository.loadStocks(stocks);
		return repository;
	}

	@Bean
	public RecipeRepository recipeRepository() {
		final RecipeRepository recipeRepository = new RecipeRepository();
		recipeRepository.loadRecipes(recipes);
		return recipeRepository;
	}
}
