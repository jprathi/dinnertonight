package com.tonis.kitchen.domain;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.google.common.collect.ImmutableMap;

public class Recipe {

	private final String dishName;
	private Map<Ingredient, Portion> ingredientPortions;
	private List<Portion> portions;

	private Recipe(final String dishName, final List<Portion> ingredientPortions) {
		super();
		this.dishName = dishName;
		addIngredientPortions(ingredientPortions);

	}

	private void addIngredientPortions(final List<Portion> portions) {
		ingredientPortions = new HashMap<Ingredient, Portion>();
		this.portions = portions;
		for (final Portion portion : portions) {
			ingredientPortions.put(portion.getIngredient(), portion);
		}
	}

	public static Recipe create(final String dishName, final List<Portion> ingredientPortions) {
		checkArgument(StringUtils.hasText(dishName), "dishName is empty");
		checkArgument(ingredientPortions != null && ingredientPortions.size() > 0, "Recipe potions is empty");
		return new Recipe(dishName, ingredientPortions);
	}

	public String getDishName() {
		return dishName;
	}

	public Map<Ingredient, Portion> getIngredientPortions() {
		return ImmutableMap.copyOf(ingredientPortions);
	}

	public List<Portion> getPortions() {
		return portions;
	}

	@Override
	public String toString() {
		return "Recipe [dishName=" + dishName + ", portions=" + portions + "]";
	}

}
