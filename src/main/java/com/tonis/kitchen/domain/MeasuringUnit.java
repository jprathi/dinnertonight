package com.tonis.kitchen.domain;

public enum MeasuringUnit {
	OF("of"),
	GRAMS("grams"), 
	ML("grams"), 
	SLICES("slices");
	
	private String description;
	
	private MeasuringUnit(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}
