package com.tonis.kitchen.domain;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

import org.springframework.util.StringUtils;

public class Ingredient {

	private static final String illegalArgErrorMessage = "Ingredient %s is empty";
	private final String name;
	private final MeasuringUnit unit;

	private Ingredient(final String name, final MeasuringUnit unit) {
		this.name = name;
		this.unit = unit;
	}

	public static Ingredient create(final String name, final MeasuringUnit unit) {
		checkArgument(StringUtils.hasText(name), illegalArgErrorMessage, "name");
		checkNotNull(unit, illegalArgErrorMessage, "unit of measure");
		return new Ingredient(name, unit);
	}

	public String getName() {
		return name;
	}

	public MeasuringUnit getUnit() {
		return unit;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, unit);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Ingredient other = (Ingredient) obj;
		return Objects.equals(name, other.getName()) && Objects.equals(unit, other.getUnit());
	}

	@Override
	public String toString() {
		return "Ingredient [name=" + name + "]";
	}

}
