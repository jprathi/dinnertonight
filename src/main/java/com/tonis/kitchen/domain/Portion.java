package com.tonis.kitchen.domain;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

public class Portion {
	private final Ingredient ingredient;
	private final Integer quantity;

	public Portion(final Ingredient ingredient, final Integer quantity) {
		super();
		this.ingredient = ingredient;
		this.quantity = quantity;
	}

	public static Portion create(final Ingredient ingredient, final Integer quantity) {
		checkNotNull(ingredient, "Ingredient is null");
		checkArgument(quantity != null && quantity > 0, "Quantity is empty");
		return new Portion(ingredient, quantity);
	}

	public Ingredient getIngredient() {
		return ingredient;
	}

	public Integer getQuantity() {
		return quantity;
	}

	@Override
	public String toString() {
		return "Portion [ingredient=" + ingredient + ", quantity=" + quantity + "]";
	}

}
