package com.tonis.kitchen.domain;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

import org.joda.time.LocalDate;

public class Stock {
	private final Ingredient ingredient;
	private final Integer quantity;
	private final LocalDate useByDate;

	private Stock(final Ingredient ingredient, final Integer quantity, final LocalDate useByDate) {
		super();
		this.ingredient = ingredient;
		this.quantity = quantity;
		this.useByDate = useByDate;
	}

	public static Stock create(final Ingredient ingredient, final Integer quantity, final LocalDate useByDate) {
		checkNotNull(ingredient, "Ingredient is null");
		checkArgument(quantity != null && quantity > 0, "Quantity is empty");
		checkNotNull(useByDate, "useByDate is empty");
		return new Stock(ingredient, quantity, useByDate);
	}

	public Ingredient getIngredient() {
		return ingredient;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public LocalDate getUseByDate() {
		return useByDate;
	}

	@Override
	public int hashCode() {
		return Objects.hash(ingredient, quantity, useByDate);
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Stock other = (Stock) obj;
		return Objects.equals(ingredient, other.getIngredient()) && Objects.equals(quantity, other.getQuantity())
				&& Objects.equals(useByDate, other.getUseByDate());
	}

	@Override
	public String toString() {
		return "Stock [ingredient=" + ingredient + ", quantity=" + quantity + ", useByDate=" + useByDate + "]";
	}

}
