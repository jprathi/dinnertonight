package com.tonis.kitchen.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.base.Predicate;
import com.google.common.collect.Maps;
import com.tonis.kitchen.domain.Ingredient;
import com.tonis.kitchen.domain.Stock;

public class StockRepository {

	private static final Logger LOGGER = LoggerFactory.getLogger(StockRepository.class);

	private final Map<Ingredient, Stock> ingredientStocks = new HashMap<>();

	public Map<Ingredient, Stock> getFreshStocksAsAt(final LocalDate asAtDate) {
		final Map<Ingredient, Stock> freshStocks = Maps.filterValues(ingredientStocks, new Predicate<Stock>() {

			@Override
			public boolean apply(final Stock input) {
				return input.getUseByDate().isAfter(asAtDate.minusDays(1));
			}
		});
		return freshStocks;
	}

	public void loadStocks(final List<Stock> stocks) {
		if (!CollectionUtils.isEmpty(stocks)) {
			for (final Stock stock : stocks) {
				addStock(stock);
			}
			LOGGER.info("finished loading {} stocks", ingredientStocks.size());
		} else {
			LOGGER.error("No Stocks to load. Restart application with valid Fridge CSV file");
		}
	}

	private void addStock(final Stock stock) {
		ingredientStocks.put(stock.getIngredient(), stock);
	}

	public Map<Ingredient, Stock> getIngredientStocks() {
		return ingredientStocks;
	}

}
