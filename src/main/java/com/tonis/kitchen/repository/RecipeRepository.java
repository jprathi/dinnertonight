package com.tonis.kitchen.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.SetMultimap;
import com.tonis.kitchen.domain.Ingredient;
import com.tonis.kitchen.domain.Recipe;

public class RecipeRepository {
	private static final Logger LOGGER = LoggerFactory.getLogger(RecipeRepository.class);
	private List<Recipe> recipes;
	private SetMultimap<Ingredient, Recipe> recipiesByIngredient;

	public void loadRecipes(final List<Recipe> recipes) {
		if (!CollectionUtils.isEmpty(recipes)) {
			this.recipes = new ArrayList<Recipe>();
			recipiesByIngredient = HashMultimap.create();
			for (final Recipe recipe : recipes) {
				addRecipe(recipe);
			}
			LOGGER.info("finished loading {} recipes", recipes.size());
		} else {
			LOGGER.error("No Recipes to load. Restart application with valid recipes JSON file");
		}

	}

	public void addRecipe(final Recipe recipe) {
		recipes.add(recipe);
		mapIngredientToRecipes(recipe);
	}

	private void mapIngredientToRecipes(final Recipe recipe) {
		for (final Ingredient ingredient : recipe.getIngredientPortions().keySet()) {
			recipiesByIngredient.put(ingredient, recipe);
		}
	}

	public List<Recipe> getAll() {
		return recipes;
	}

	public Set<Recipe> getRecipesWith(final Ingredient ingredient) {
		return ImmutableSet.copyOf(recipiesByIngredient.get(ingredient));
	}
}
