package com.tonis.kitchen.utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tonis.kitchen.domain.Ingredient;
import com.tonis.kitchen.domain.MeasuringUnit;
import com.tonis.kitchen.domain.Portion;
import com.tonis.kitchen.domain.Recipe;

public class RecipeJSONReader {

	private final String fileLocation;

	public RecipeJSONReader(final String fileLocation) {
		super();
		this.fileLocation = fileLocation;
	}

	public List<Recipe> read() throws Exception {
		final ObjectMapper mapper = new ObjectMapper();
		final List<RecipeDTO> recipeList = mapper.readValue(new File(fileLocation), new TypeReference<List<RecipeJSONReader.RecipeDTO>>() {
		});
		List<Portion> portions;
		final List<Recipe> recipes = new ArrayList<Recipe>();
		for (final RecipeDTO recipeDTO : recipeList) {
			portions = new ArrayList<Portion>();
			for (final IngredientDTO ingredientDTO : recipeDTO.getIngredients()) {
				portions.add(Portion.create(Ingredient.create(ingredientDTO.getItem(), MeasuringUnit.valueOf(ingredientDTO.getUnit().toUpperCase())),
						ingredientDTO.getAmount()));
			}
			recipes.add(Recipe.create(recipeDTO.getName(), portions));
		}
		return recipes;
	}

	private static class RecipeDTO {
		private String name;
		private List<IngredientDTO> ingredients;

		public String getName() {
			return name;
		}

		public void setName(final String name) {
			this.name = name;
		}

		public List<IngredientDTO> getIngredients() {
			return ingredients;
		}

		public void setIngredients(final List<IngredientDTO> ingredients) {
			this.ingredients = ingredients;
		}

	}

	private static class IngredientDTO {
		private String item;
		private Integer amount;
		private String unit;

		public String getItem() {
			return item;
		}

		public void setItem(final String item) {
			this.item = item;
		}

		public Integer getAmount() {
			return amount;
		}

		public void setAmount(final Integer amount) {
			this.amount = amount;
		}

		public String getUnit() {
			return unit;
		}

		public void setUnit(final String unit) {
			this.unit = unit;
		}

	}
}
