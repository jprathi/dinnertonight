package com.tonis.kitchen.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.tonis.kitchen.domain.Ingredient;
import com.tonis.kitchen.domain.MeasuringUnit;
import com.tonis.kitchen.domain.Stock;

public class StocksCSVReader {

	private static final Logger LOG = LoggerFactory.getLogger(StocksCSVReader.class);
	private final String fileLocation;

	public StocksCSVReader(final String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public List<Stock> read() throws Exception {
		final List<Stock> readStocks = new ArrayList<Stock>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(fileLocation));
			String line;
			String[] stocks;
			Stock stock;
			final DateTimeFormatter formatter = DateTimeFormat.forPattern("d/M/yyyy");
			while ((line = br.readLine()) != null) {
				stocks = line.split(",");
				if (stocks.length == 4) {
					stock = Stock.create(Ingredient.create(stocks[0], MeasuringUnit.valueOf(stocks[2].toUpperCase())), Integer.valueOf(stocks[1]),
							LocalDate.parse(stocks[3], formatter));
					readStocks.add(stock);
				}
			}
		} catch (final Exception e) {
			LOG.error(e.getMessage());
			throw e;
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
		}
		return readStocks;
	}
}
