package com.tonis.kitchen.service;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Ordering;
import com.tonis.kitchen.domain.Ingredient;
import com.tonis.kitchen.domain.Portion;
import com.tonis.kitchen.domain.Recipe;
import com.tonis.kitchen.domain.Stock;
import com.tonis.kitchen.repository.RecipeRepository;
import com.tonis.kitchen.repository.StockRepository;

@Service
public class KitchenService {

	public static final String ORDER_OUT = "Order out today!";

	Set<Recipe> disqualifiedRecipes = new HashSet<Recipe>();

	private static final Ordering<Map.Entry<Ingredient, Stock>> useByDateOrdering = Ordering.from(new Comparator<Map.Entry<Ingredient, Stock>>() {

		@Override
		public int compare(final Entry<Ingredient, Stock> o1, final Entry<Ingredient, Stock> o2) {
			return o1.getValue().getUseByDate().compareTo(o2.getValue().getUseByDate());
		}
	});

	@Autowired
	private StockRepository stockRepository;

	@Autowired
	private RecipeRepository recipeRepository;

	public String getDinnerSuggestion(final LocalDate asAtDate) {
		String suggestion = null;
		final Map<Ingredient, Stock> freshStocks = stockRepository.getFreshStocksAsAt(asAtDate);
		if (!CollectionUtils.isEmpty(freshStocks)) {
			final Map<Ingredient, Stock> orderedStocks = sortStocksByUseByDate(freshStocks);
			for (final Ingredient ingredient : orderedStocks.keySet()) {
				final Set<Recipe> recipesWithIngredient = recipeRepository.getRecipesWith(ingredient);
				suggestion = findRecipeToSuggest(recipesWithIngredient, orderedStocks);
				if (StringUtils.hasText(suggestion)) {
					break;
				}
			}
		}
		return suggestion;
	}

	private String findRecipeToSuggest(final Set<Recipe> recipesWithIngredient, final Map<Ingredient, Stock> orderedStocks) {
		String suggestion = null;
		for (final Recipe recipe : recipesWithIngredient) {
			if (disqualifiedRecipes.contains(recipe)) {
				continue;
			}
			if (recipeHasRequiredStocks(recipe, orderedStocks)) {
				suggestion = recipe.getDishName();
				break;
			} else {
				disqualifiedRecipes.add(recipe);
			}
		}
		return suggestion;
	}

	private boolean recipeHasRequiredStocks(final Recipe recipe, final Map<Ingredient, Stock> orderedStocks) {
		final Ingredient ingredient;
		Stock stock;
		boolean hasAll = true;
		for (final Portion portion : recipe.getPortions()) {
			stock = orderedStocks.get(portion.getIngredient());
			if (stock == null || portion.getQuantity() > stock.getQuantity()) {
				hasAll = false;
				break;
			}
		}
		return hasAll;
	}

	private Map<Ingredient, Stock> sortStocksByUseByDate(final Map<Ingredient, Stock> freshStocks) {
		final ImmutableMap.Builder<Ingredient, Stock> orderedStocksBuilder = ImmutableMap.builder();
		for (final Entry<Ingredient, Stock> stockEntry : useByDateOrdering.sortedCopy(freshStocks.entrySet())) {
			orderedStocksBuilder.put(stockEntry);
		}
		return orderedStocksBuilder.build();
	}

	public Map<String, Integer> getInventoryDetails() {
		final Map<String, Integer> inventory = new HashMap<>();
		inventory.put("Loaded Stocks", stockRepository.getIngredientStocks().size());
		inventory.put("Loaded Recipes", recipeRepository.getAll().size());
		return inventory;
	}
}
