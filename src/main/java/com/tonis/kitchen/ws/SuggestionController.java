package com.tonis.kitchen.ws;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tonis.kitchen.service.KitchenService;

@RestController
public class SuggestionController {

	@Autowired
	private KitchenService kitchenService;
	private final DateTimeFormatter formatter = DateTimeFormat.forPattern("d/M/yyyy");

	@RequestMapping("/tonis-kitchen/suggestion")
	public Map<String, Object> getSuggestion(@RequestParam(value = "date", required = false) final String dateStr) {
		final LocalDate asAtDate = StringUtils.isEmpty(dateStr) ? LocalDate.now() : LocalDate.parse(dateStr, formatter);

		final Map<String, Object> map = new HashMap<>();
		map.put("greeting", "Hello Diners!");
		map.put("inventory", kitchenService.getInventoryDetails());
		map.put("suggestion", kitchenService.getDinnerSuggestion(asAtDate));
		return map;
	}
}
